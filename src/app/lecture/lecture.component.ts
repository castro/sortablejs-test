import { Component, Input, OnInit } from '@angular/core';
import { SortablejsOptions } from 'angular-sortablejs';

@Component({
  selector: 'eb-lecture',
  templateUrl: './lecture.component.html',
  styleUrls: ['./lecture.component.scss'],
})
export class EbLectureComponent implements OnInit {
  @Input() lecture;
  contentsSortablejsOptions: SortablejsOptions;
  constructor() {}

  ngOnInit() {
    console.log(this.lecture);

    this.contentsSortablejsOptions = {
      draggable: 'eb-lecture-content',
      handle: '.eb-lecture-content-wrapper',
      group: `contents${this.lecture.id}`,
      ghostClass: 'sortable-ghost',
      onRemove: event => {
        console.log(event);
      },
      onUpdate: (event: any) => {
        console.log(event);
      },
      onAdd: (event: any) => {
        console.log(event);
      },
    };
  }
}
