import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EbLectureComponent } from './lecture.component';

describe('EbLectureComponent', () => {
  let component: EbLectureComponent;
  let fixture: ComponentFixture<EbLectureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EbLectureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EbLectureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
