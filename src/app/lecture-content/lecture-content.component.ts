import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'eb-lecture-content',
  templateUrl: './lecture-content.component.html',
  styleUrls: ['./lecture-content.component.scss']
})
export class LectureContentComponent implements OnInit {
  @Input() content;

  constructor() { }

  ngOnInit() {
    console.log(this.content);
  }

}
