import { Component } from '@angular/core';
import { SortablejsOptions } from 'angular-sortablejs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  modulesList = [
    {
      title: 'm1',
      id: '5c91176f34fc8500254976a8',
      lectures: [
        {
          slug: 'a1-ponto-1',
          title: 'a1.1',
          id: '5c8fa4295cd4230046d0fe38',
          contents: [],
        },
        {
          slug: 'a1-ponto-2-1',
          title: 'a1.2',
          id: '5cacdf18d1a706002e38ec62',
          contents: [
            {
              id: 0,
            },
            {
              id: 1,
            },
            {
              id: 2,
            },
          ],
        },
      ],
    },
    {
      title: 'm2',
      description: null,
      position: 1,
      id: '5cacd659d1a706005638ea4f',
      type: 'course--module',
      lectures: [
        {
          createdAt: '2019-04-09T15:06:27.311-03:00',
          slug: 'a2-ponto-1-1',
          description: null,
          position: 0,
          title: 'a2.1',
          updatedAt: '2019-04-09T15:06:33.135-03:00',
          extra: false,
          minimumGrade: -1,
          publishedAt: null,
          alwaysReleased: true,
          releasesAt: null,
          releasesAtFuture: null,
          releasesIn: null,
          showForum: true,
          free: false,
          private: false,
          amountOfContents: 0,
          id: '5cacdf23d1a706002e38ec64',
          type: 'course--lecture',
          lectureStatus: null,
          attendances: [],
          contents: [],
          simpleContents: [],
          subscriptions: [],
          teams: [],
          permissions: [null, null, null, null, null, null],
        },
        {
          createdAt: '2019-03-14T14:33:18.467-03:00',
          slug: 'a2-ponto-2',
          description: null,
          position: 1,
          title: 'a2.2',
          updatedAt: '2019-04-09T14:29:33.849-03:00',
          extra: false,
          minimumGrade: -1,
          publishedAt: null,
          alwaysReleased: true,
          releasesAt: null,
          releasesAtFuture: null,
          releasesIn: null,
          showForum: true,
          free: false,
          private: false,
          amountOfContents: 0,
          id: '5c8a905e4dd5e00026deaec9',
          type: 'course--lecture',
          lectureStatus: null,
          attendances: [],
          contents: [],
          simpleContents: [],
          subscriptions: [],
          teams: [],
          permissions: [null, null, null, null, null, null],
        },
      ],
    },
    {
      title: 'm3',
      lectures: [],
    },
  ];
  modulesSortableOptions: SortablejsOptions = {
    draggable: 'eb-module',
    handle: '.eb-module-wrapper',
    group: 'modules',
    ghostClass: 'sortable-ghost',
    onRemove: event => {
      console.log(event);
    },
    onUpdate: (e: any) => {
      console.log(event);
    },
    onAdd: (event: any) => {
      console.log(event);
    },
  };

  lecturesList = [
    {
      slug: 'a1-ponto-1',
      title: 'a1.1',
      id: '5c8fa4295cd4230046d0fe38',
      contents: [
        {
          id: 0,
        },
        {
          id: 1,
        },
        {
          id: 2,
        },
      ],
    },
    {
      slug: 'a1-ponto-2-1',
      title: 'a1.2',
      id: '5cacdf18d1a706002e38ec62',
      contents: [],
    },
    {
      slug: 'a2-ponto-1-1',
      title: 'a2.1',
      id: '5cacdf23d1a706002e38ec64',
      contents: [],
    },
    {
      slug: 'a2-ponto-2',
      title: 'a2.2',
      id: '5c8a905e4dd5e00026deaec9',
      contents: [],
    },
  ];
  lecturesSortablejsOptions: SortablejsOptions = {
    draggable: 'eb-lecture',
    handle: '.eb-lecture-wrapper',
    group: 'lectures',
    ghostClass: 'sortable-ghost',
    onRemove: event => {
      console.log(event);
    },
    onUpdate: (event: any) => {
      console.log(event);
    },
    onAdd: (event: any) => {
      console.log(event);
    },
  };
}
