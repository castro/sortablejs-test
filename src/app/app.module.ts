import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SortablejsModule } from 'angular-sortablejs';
import { AppComponent } from './app.component';
import { EbModuleComponent } from './module/module.component';
import { EbLectureComponent } from './lecture/lecture.component';
import { LectureContentComponent } from './lecture-content/lecture-content.component';

@NgModule({
  declarations: [
    AppComponent,
    EbModuleComponent,
    EbLectureComponent,
    LectureContentComponent
  ],
  imports: [
    BrowserModule,
    SortablejsModule.forRoot({
      animation: 250,
      scroll: true,
      scrollSensitivity: 100,
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
