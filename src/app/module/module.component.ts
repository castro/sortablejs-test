import { Component, Input, OnInit } from '@angular/core';
import { SortablejsOptions } from 'angular-sortablejs';

@Component({
  selector: 'eb-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.scss'],
})
export class EbModuleComponent implements OnInit {
  @Input() module;

  lecturesSortablejsOptions: SortablejsOptions = {
    draggable: 'eb-lecture',
    handle: '.eb-lecture-wrapper',
    group: 'lectures',
    ghostClass: 'sortable-ghost',
    onRemove: event => {
      console.log(event);
    },
    onUpdate: (event: any) => {
      console.log(event);
    },
    onAdd: (event: any) => {
      console.log(event);
    },
  };
  constructor() {}

  ngOnInit() {
    console.log(this.module);
  }
}
