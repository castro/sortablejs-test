import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EbModuleComponent } from './module.component';

describe('EbModuleComponent', () => {
  let component: EbModuleComponent;
  let fixture: ComponentFixture<EbModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EbModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EbModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
